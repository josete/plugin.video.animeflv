from bs4 import BeautifulSoup
import requests
import json
import xbmcgui
import xbmc
import time

ANIMEFLV="https://animeflv.net"
headers = requests.utils.default_headers()
headers.update({
    'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0',
})

def get_episodios_recientes():
    r = requests.get("http://localhost:3000/recientes",headers=headers)
    #xbmc.log(str(r.text),level=xbmc.LOGNOTICE) 
    #if r.status_code == 200:
     #   html = r.text
      #  soup = BeautifulSoup(html, 'html.parser')
       # episodios = soup.find("ul",{"class": "ListEpisodios"})
        #listaEpisodios = []
        #for episodio in episodios.find_all("li"):
         #   e = {}
          #  e["enlace"] = get_url_video(ANIMEFLV+episodio.a["href"])
           # e["imagen"] = ANIMEFLV+episodio.a.find("span",{"class": "Image"}).img["src"]
            #e["titulo"] = episodio.a.strong.text + " -- " + episodio.a.find("span",{"class": "Capi"}).text
            #listaEpisodios.append(e)
            #xbmcgui.Dialog().notification("Info",str(episodio.a.strong.text))    
        #xbmcgui.Dialog().notification("Info",str(listaEpisodios))
    return r.json()["episodios"]
    pass

def buscar_anime(nombre):
    r = requests.get("http://localhost:3000/buscar?nombre="+nombre,headers=headers)
    #if r.status_code == 200:
     #   html = r.text
      #  soup = BeautifulSoup(html, 'html.parser')
       # animes = soup.find("ul",{"class": "ListAnimes"})
        #listaAnimes = []
        #for anime in animes.find_all("li"):           
         #   a = {}
          #  a["titulo"] = anime.article.a.h3.text
           # a["enlace"] = ANIMEFLV+anime.article.a["href"]
            #a["imagen"] = anime.article.a.find("div",{"class": "Image"}).figure.img["src"]
            #listaAnimes.append(a)
    return r.json()["animes"]            
    pass

def explorar_animes(pagina):
    r = requests.get("http://localhost:3000/explorar?pagina="+str(pagina),headers=headers)
    #if r.status_code == 200:
     #   html = r.text
      #  soup = BeautifulSoup(html, 'html.parser')
       # animes = soup.find("ul",{"class": "ListAnimes"})
        #listaAnimes = []
        #for anime in animes.find_all("li"):           
         #   a = {}
          #  a["titulo"] = anime.article.a.h3.text
           # a["enlace"] = ANIMEFLV+anime.article.a["href"]
            #a["imagen"] = anime.article.a.find("div",{"class": "Image"}).figure.img["src"]
            #listaAnimes.append(a)
    return r.json()["animes"]            
    pass

def get_tope_explorar():
    r = requests.get("http://localhost:3000/tope",headers=headers)
    #if r.status_code == 200:
     #   html = r.text
      #  soup = BeautifulSoup(html, 'html.parser')
       # botones = soup.find("ul",{"class": "pagination"})        
        #penultimo = botones.find_all("li")[len(botones.find_all("li"))-2]
    return r.json()["tope"]
    pass

def get_episodios_anime(url):
    r = requests.get("http://localhost:3000/episodios?url="+url)
    #if r.status_code == 200:        
     #   html = r.text
      #  soup = BeautifulSoup(html, 'html.parser')
       # scripts = soup.find_all("script")
        #listaEpisodios = []
        #for script in scripts:
         #   texto = script.text
          #  if "anime_info" in texto:
           #     info = json.loads(texto.split("var anime_info =")[1].split("var episodes =")[0].replace(";",""))
            #    episodios = json.loads(texto.split("var episodes =")[1].split("var last_seen")[0].replace(";",""))
             #   for episodio in episodios:
              #      e = {}
                    #e["enlace"] = get_url_video(ANIMEFLV+"/ver/"+str(episodio[1])+"/"+str(info[2])+"-"+str(episodio[0]))
               #     e["nombre"] = info[1]
                #    e["enlaceAnime"] = url
                 #   e["enlace"] = ANIMEFLV+"/ver/"+str(episodio[1])+"/"+str(info[2])+"-"+str(episodio[0])
                  #  e["imagen"] = "https://cdn.animeflv.net/screenshots/"+str(info[0])+"/"+str(episodio[0])+"/th_3.jpg"
                   # e["titulo"] = "Episodio "+str(episodio[0])
                    #listaEpisodios.append(e)
                    #xbmc.log(str(e),level=xbmc.LOGNOTICE)  
    return r.json()["episodios"] 
    pass

def get_urls_anime(url):
    r = requests.get(url)
    if r.status_code == 200:        
        html = r.text
        soup = BeautifulSoup(html, 'html.parser')
        scripts = soup.find_all("script")
        listaEpisodios = []
        for script in scripts:
            texto = script.text
            if "anime_info" in texto:
                info = json.loads(texto.split("var anime_info =")[1].split("var episodes =")[0].replace(";",""))
                episodios = json.loads(texto.split("var episodes =")[1].split("var last_seen")[0].replace(";",""))
                for episodio in episodios:                    
                    listaEpisodios.append(get_url_video(ANIMEFLV+"/ver/"+str(episodio[1])+"/"+str(info[2])+"-"+str(episodio[0])))
                    #xbmc.log(str(e),level=xbmc.LOGNOTICE)  
                return listaEpisodios
    pass

def play_episodio(enlace):
    r = requests.get("http://localhost:3000/play?enlace="+enlace)
    return r.json()["enlace"]

def get_url_video(url):
    r = requests.get(url)
    if r.status_code == 200:
        html = r.text
        soup = BeautifulSoup(html, 'html.parser')
        scripts = soup.find_all("script")
        for script in scripts:
            texto = script.text
            if "anime_id" in texto:
                enlaces = texto.split("var videos =")[1].split("$(document)")[0].replace(";","")
                enlacesJson = json.loads(enlaces)["SUB"]
                natsuki = enlacesJson[0]["code"]
                natsukiEnlace = natsuki.split("src=")[1].split("scrolling=")[0].replace("\\","").replace('"',"")
                r2 = requests.get(natsukiEnlace)
                if r2.status_code == 200:
                    html2 = r2.text
                    soup2 = BeautifulSoup(html2, 'html.parser')
                    scripts2 = soup2.find_all("script")
                    for script2 in scripts2:
                        texto2 = script2.text
                        if "var check_url" in texto2:
                            enlaceCheck = "https://s1.animeflv.net/"+texto2.split("var check_url =")[1].replace(";","").replace("'","").strip()
                            r3 = requests.get(enlaceCheck)
                            if r3.status_code == 200:
                                video = r3.json()["file"]
                                return video                                
                            #xbmcgui.Dialog().notification("Info",str(enlaceCheck))                                
    pass

