import sys
from urllib import urlencode
from urlparse import parse_qsl
import xbmcgui
import xbmcplugin
import recientes
import biblioteca
import xbmc

# Get the plugin url in plugin:// notation.

_url = sys.argv[0]

# Get the plugin handle as an integer number.

_handle = int(sys.argv[1])


OPCIONES = ["Episodios Recientes","Explorar","Buscar"]

explorar_pagina = 1
tope_explorar = -1

def get_url(**kwargs):
    return '{0}?{1}'.format(_url, urlencode(kwargs))


def get_recientes():    
    episodios = recientes.get_episodios_recientes()
    list_videos(episodios)
    #xbmcgui.Dialog().notification("Info",str(episodios))

def buscar():
    nombre = xbmcgui.Dialog().input("Nombre",type=xbmcgui.INPUT_ALPHANUM) 
    lista = recientes.buscar_anime(nombre)
    listar_animes(lista)
    pass

def explorar():
    lista = recientes.explorar_animes(explorar_pagina)
    global tope_explorar  
    if tope_explorar == -1:
        tope_explorar = recientes.get_tope_explorar()
    listar_animes_explorar(lista)
    pass

def listar_episodios_anime(enlace):
    episodios = recientes.get_episodios_anime(enlace)
    list_videos_anime(episodios)
    pass

def get_videos(category):
    return VIDEOS[category]

def main_menu():
    xbmcplugin.setPluginCategory(_handle, 'AnimeFLV')
    xbmcplugin.setContent(_handle, 'videos')
    for opcion in OPCIONES:
        list_item = xbmcgui.ListItem(label=opcion)
        list_item.setInfo('video', {'title': opcion,
                                    'mediatype': 'video'})
        url = get_url(action='listing', category=opcion)
        is_folder = True
        xbmcplugin.addDirectoryItem(_handle, url, list_item, is_folder)
    xbmcplugin.addSortMethod(_handle, xbmcplugin.SORT_METHOD_NONE)
    xbmcplugin.endOfDirectory(_handle)

def list_videos(episodios):
    xbmcplugin.setPluginCategory(_handle, "Episodios")    
    xbmcplugin.setContent(_handle, 'videos')
    for episodio in episodios:        
        list_item = xbmcgui.ListItem(label=episodio['titulo'])
        list_item.setInfo('video', {'title': episodio['titulo'],
                                    'mediatype': 'video'})
        list_item.setArt({'thumb': episodio['imagen'], 'icon': episodio['imagen'], 'fanart': episodio['imagen']})
        list_item.setProperty('IsPlayable', 'true')
        url = get_url(action='play', video=episodio['enlace'])
        is_folder = False
        xbmcplugin.addDirectoryItem(_handle, url, list_item, is_folder)
    xbmcplugin.addSortMethod(_handle, xbmcplugin.SORT_METHOD_NONE)
    xbmcplugin.endOfDirectory(_handle)

def list_videos_anime(episodios):
    xbmcplugin.setPluginCategory(_handle, episodios[0]['nombre'])    
    xbmcplugin.setContent(_handle, 'videos')
    for episodio in episodios:        
        list_item = xbmcgui.ListItem(label=episodio['titulo'])
        list_item.setInfo('video', {'title': episodio['titulo'],
                                    'mediatype': 'video'})
        list_item.setArt({'thumb': episodio['imagen'], 'icon': episodio['imagen'], 'fanart': episodio['imagen']})
        list_item.setProperty('IsPlayable', 'true')
        url = get_url(action='play_anime', video=episodio['enlace'])
        is_folder = False
        xbmcplugin.addDirectoryItem(_handle, url, list_item, is_folder)
    list_item = xbmcgui.ListItem(label="Incluir en la biblioteca")
    url = get_url(action='anadirBiblioteca', enlace=episodios[0]['enlaceAnime'],nombre=episodios[0]['nombre'])
    is_folder = False
    xbmcplugin.addDirectoryItem(_handle, url, list_item, is_folder)
    xbmcplugin.addSortMethod(_handle, xbmcplugin.SORT_METHOD_NONE)
    xbmcplugin.endOfDirectory(_handle)

def listar_animes(animes):
    xbmcplugin.setPluginCategory(_handle, 'Animes')
    xbmcplugin.setContent(_handle, 'videos')
    for anime in animes:
        list_item = xbmcgui.ListItem(label=anime["titulo"])
        list_item.setInfo('video', {'title': anime["titulo"],
                                    'mediatype': 'video'})
        list_item.setArt({'thumb': anime['imagen'], 'icon': anime['imagen'], 'fanart': anime['imagen']})
        url = get_url(action='listing', category="Episodios", enlace=anime["enlace"])
        is_folder = True
        xbmcplugin.addDirectoryItem(_handle, url, list_item, is_folder)    
    xbmcplugin.addSortMethod(_handle, xbmcplugin.SORT_METHOD_NONE)
    xbmcplugin.endOfDirectory(_handle)

def listar_animes_explorar(animes):
    xbmcplugin.setPluginCategory(_handle, 'Animes')
    xbmcplugin.setContent(_handle, 'videos')
    for anime in animes:
        list_item = xbmcgui.ListItem(label=anime["titulo"])
        list_item.setInfo('video', {'title': anime["titulo"],
                                    'mediatype': 'video'})
        list_item.setArt({'thumb': anime['imagen'], 'icon': anime['imagen'], 'fanart': anime['imagen']})
        url = get_url(action='listing', category="Episodios", enlace=anime["enlace"])
        is_folder = True
        xbmcplugin.addDirectoryItem(_handle, url, list_item, is_folder)
    if explorar_pagina < tope_explorar:
        list_item = xbmcgui.ListItem(label="Siguiente >>")
        url = get_url(action='listing', category="anime++")
        xbmcplugin.addDirectoryItem(_handle, url, list_item, is_folder)
    if explorar_pagina > 1:
        list_item = xbmcgui.ListItem(label="Anterior <<")
        url = get_url(action='listing', category="anime--")
        xbmcplugin.addDirectoryItem(_handle, url, list_item, is_folder)
    xbmcplugin.addSortMethod(_handle, xbmcplugin.SORT_METHOD_NONE)
    xbmcplugin.endOfDirectory(_handle)

def play_video(path):
    play_item = xbmcgui.ListItem(path=path)
    xbmcplugin.setResolvedUrl(_handle, True, listitem=play_item)

def router(paramstring):
    params = dict(parse_qsl(paramstring))
    global explorar_pagina
    if params:
        if params['action'] == 'listing':
            if params['category'] == "Episodios Recientes":                
                get_recientes()
            elif params['category'] == "Explorar":
                explorar()
            elif params['category'] == "Buscar":
                buscar()
            elif params['category'] == "Episodios":
                listar_episodios_anime(params['enlace'])
            elif params['category'] == "anime++":
                explorar_pagina+=1
                xbmc.log(str("+1"),level=xbmc.LOGNOTICE)  
                explorar()
            elif params['category'] == "anime--":
                explorar_pagina-=1
                explorar()
        elif params['action'] == 'play':            
            play_video(params['video'])
        elif params['action'] == 'play_anime':            
            play_video(recientes.play_episodio(params['video']))
        elif params['action'] == "anadirBiblioteca":
            biblioteca.crear_strm(params['enlace'],params['nombre'])
        else:
            raise ValueError('Invalid paramstring: {0}!'.format(paramstring))
    else:        
        main_menu()

if __name__ == '__main__':    
    router(sys.argv[2][1:])