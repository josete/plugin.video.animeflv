import xbmcgui
import xbmc
import xbmcaddon
import os
import recientes

def crear_strm(enlace,nombre):
    __settings__   = xbmcaddon.Addon(id="plugin.video.animeflv")
    directorio_animes = __settings__.getSetting("directorioAnimes")    
    if not os.path.exists(directorio_animes+"/"+nombre):
        os.makedirs(directorio_animes+"/"+nombre.replace(":",""))
    if os.path.exists(directorio_animes+"/"+nombre.replace(":","")):
        videos = recientes.get_urls_anime(enlace)
        numero = len(videos)
        actual = 0
        progreso = xbmcgui.DialogProgressBG()
        progreso.create("Incluyendo en la biblioteca")
        for video in videos:
            file = open(directorio_animes+"/"+nombre.replace(":","")+"/"+"1x"+str("{0:0>2}".format(numero))+".strm","w+")
            file.write(video)
            file.close()
            if numero-1>0:
                numero-=1            
            actual+=1
            progreso.update((actual/numero)*100)
        progreso.close()
        #xbmc.log(str(videos),level=xbmc.LOGNOTICE)          
    #xbmcgui.Dialog().notification("Info",str(nombre))    
    pass